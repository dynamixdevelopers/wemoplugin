# The Wemo Plugin
The Wemo plugin offers support for Wemo smart plugs and motion sensors.

All interaction is performed through the Ambient Control mechanisms. See [Ambient Control documentation](https://bitbucket.org/dynamixdevelopers/ambientcontrolplugin)

The plugins control description is as follows:
```JSON
{
    "name":"wemoplugin",
    "artifact_id":"org.ambientdynamix.contextplugins.wemoplugin",
    "description":"",
    "pluginVersion":"1.0.0",
    "owner_id":"",
    "platform":"",
    "minPlatformVersion":"",
    "minFrameworkVersion":"",
    "inputList":[
        {
            "mandatoryControls":[
                "SWITCH"
            ],
            "priority":1
        }
    ],
    "outputList":{
        "Switch":"SWITCH"
    },
    "optionalInputList":[]
}
```